﻿using NUnit.Framework;
using SimpleStringCalculator;

namespace StringCalculatorTests
{
    [TestFixture]
    public class StringCalculatorShould
    {
        private StringCalculator CreateCalculator()
        {
            return new StringCalculator();
        }

        [Test]
        public void IgnoresNumbers_IfInput_HasNumbers_BiggerThan_1000()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("2,1001");
            Assert.AreEqual(2, result);
        }

        [Test]
        public void ResturnsSumOfSeveralDigitalNumber_IfInput_IsSeveralDigitalNumber()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("10,20");
            Assert.AreEqual(10 + 20, result);
        }

        [Test]
        public void RetrunsZero_IfInput_IsNullOrEmpty()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add(string.Empty);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Return_1_IfInput_Is_1()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1");
            Assert.AreEqual(1, result);
        }

        [Test]
        public void ReturnsAnyNumber_IfInput_IsAnyNumber()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("2");
            Assert.AreEqual(2, result);
        }

        [Test]
        public void ReturnsDigitalNumber_IfInput_IsDigitalNumber()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("120");
            Assert.AreEqual(120, result);
        }

        [Test]
        public void ReturnsSumOfSeveralNumber_IfInput_HasCustomDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[;]\n1;2");
            Assert.AreEqual(1 + 2, result);
        }

        [Test]
        public void ReturnsSumOfSeveralNumber_IfInput_HasCustomDelimiterLongerThanOneChar()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[--]\n1--2");
            Assert.AreEqual(1 + 2, result);
        }

        [Test]
        public void ReturnsSumOfSeveralNumbers_IfInput_HasNewLineDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1\n2,3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [Test]
        public void ReturnsSumOfSeveralNumbers_IfInput_IsSeveralNumbers()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,2,3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [Test]
        public void ReturnsSumOfTwoNumbers_IfInput_IsTwoNumbers()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,2");
            Assert.AreEqual(1 + 2, result);
        }

        [Test]
        public void ReturnsZero_IfInput_HasWrongDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,\n,2");
            Assert.AreEqual(0, result);
        }

        [Test]
        public void ThorwsNegativeNumberException_IfInput_HasNegativeNumbers()
        {
            var calculator = CreateCalculator();
            Assert.Throws<NegativeNumberException>(() => calculator.Add("-1,2"));
        }

        [Test]
        public void ReturnsSumOfSeveralNumbers_IfInput_HasMultipleDelimiters()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[-][;]\n1-2;3");
            Assert.AreEqual(1+2+3,result);
        }

        [Test]
        public void ReturnsSumOfSeveralNumbers_IfInput_HasMultipleDelimitersLongerThanOneChar()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[--][**]\n1--2**3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

    }
}