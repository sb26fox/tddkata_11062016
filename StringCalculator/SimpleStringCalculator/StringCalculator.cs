﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleStringCalculator
{
    public class StringCalculator
    {
        private readonly int defaultValue = 0;
        private int result = 0;

        public int Add(string input)
        {
            if (IsEmpty(input) || HasWrongDelimiter(input))
            {
                return defaultValue;
            }

            if (HasCustomDelimiter(input))
            {
                var delimiter = GetDilimiter(input);
                var destNumbers = GetNumbers(input);

                var numbers = destNumbers.Split(delimiter,StringSplitOptions.None);

                return Count(numbers);
            }

            if (HasStandartDelimiter(input))
            {
                var numbers = Split(input);

                return Count(numbers);
            }

            return Parse(input);
        }

        private string GetNumbers(string input)
        {
            var index = input.IndexOf("\n");

            return input.Substring(index);
        }

        private bool HasCustomDelimiter(string input)
        {
            return input.StartsWith("//");
        }

        private bool HasWrongDelimiter(string input)
        {
            return input.Contains(",\n");
        }

        private string[] Split(string input)
        {
            return input.Split(',', '\n');
        }

        private bool HasStandartDelimiter(string input)
        {
            return input.Contains(",") || input.Contains('\n');
        }

        private int Count(string[] numbers)
        {
            var result = 0;
            var negativeNumbers = string.Empty;

            foreach (var number in numbers)
            {
                if (Parse(number) < 0)
                {
                    string.Concat(negativeNumbers, " ", number);
                }
            }

            foreach (var number in numbers)
            {
                if (Parse(number) < 0)
                {
                    throw new NegativeNumberException("Input has negative numbers:" + negativeNumbers);
                }
                else
                {
                    if (Parse(number) < 1000)
                    {
                        result += Parse(number);
                    }

                }
            }

            return result;
        }

        private int Parse(string input)
        {
            return int.Parse(input);
        }

        private bool IsEmpty(string input)
        {
            return string.IsNullOrEmpty(input);
        }

        private string[] GetDilimiter(string input)
        {
            var index = input.IndexOf("\n");
            List<string> delimiters=new List<string>();

            input.Remove(index);
            var del = input.Split('[', ']');

            for (int i = 1; i < del.Length - 1; i++)
            {
                delimiters.Add(del[i]);
            }

            return delimiters.ToArray();
        }
    }
}